/*
The SQLCommand package provides a simple method of chaining together database operations

Instead of the standard:

	Open Database
	Begin Transaction
		Check for Error
	Prepare Statement
	Defer Close Statement
		Check for Error
			Rollback Transaction
	Execute/Query Statement
		Check for Error
		Rollback Transaction
	Scan Results
		Check for Error
	Commit Transaction

SQLCommand allows you to do:

	Open Database
	scf := NewFactory(database,log)
		sc := scf.NewCommand(someSQL)
		sc.Begin().Prepare().Query(parameters...).Scan(pointers...).CloseAll()
		if sc.IsValid{
		// do whatever with the pointer	map or set, depending on what was executed/queried
	} else {
		// the error that caused the problem is in sc.Err, so deal with it
	}

standard patterns of use are:

	var scf SQLCommandFactory

	func init(){
		scf = new(SQLCommandFactory)
		scf.DB, err = db.Open(driver,DSN)
		if nil != err{
			panic("unable to open database")
		}
		scf.Log = log.Logger
	}
	func getData() (someDataStruct, error){
		d := new(someDataStruct)
		sc:=scf.NewCommand("SELECT * FROM someDataTable WHERE Id=?")
		sc.Begin().QueryRow().Scan(&d.Field1,&d.Field2,&d.Field3).CloseAll()
		if sc.IsValid{
			return d,nil
		}
		return nil, errors.New("error occurred while getting data: " + sc.Err.Error())
	}

	func insertData(data) error{
		//slightly extreme example... normally this would be broken into a couple of lines
		return scf.NewCommand("INSERT INTO someDataTable (Field1,Field2,Field3) VALUES (?,?,?)").Begin().Exec(data.Field1,data.Field2,data.Field3).CloseAll().Err
	}

*/
package sqlcommand

import (
	"database/sql"
	"errors"
	"log"
	"reflect"

	_ "github.com/go-sql-driver/mysql"
)

type SQLCommand struct {
	// Db is the database/sql DB object to use for this SQLCommand.
	// The SQLCommand does not manage or maintain this connection in any way
	Db *sql.DB
	// Log provides a standard log.Logger reference that is used by the SQLCommand to log events. It doesn't have to be connected to stdout if you don't want the activity spam
	Log *log.Logger
	// Sql is the SQL string to be used for the SQLCommand. It is driver-dependant and will not be corrected by SQLCommand
	Sql string
	// Tx is the database/sql transaction currently in use by the SQLCommand. It may be nil without IsValid() returning false
	Tx *sql.Tx
	// Ps is the database/sql statement currently prepared by the SQLCommand. It may be nil without IsValid() returning false
	Ps *sql.Stmt
	// Res is the database/sql result of a SQL exec execution
	Res *sql.Result
	// LastId contains the result from the GetLastId() operation on the Res value; the last id created by an Exec() execution
	LastId int64
	// RowsAffects contains the number of rows affected by the last Exec() execution.
	RowsAffected int64
	// Rows is the database/sql Rows struct returned by a SQL Query execution
	Rows *sql.Rows
	// Row is the database/sql Row struct returned by a SQL QueryRow execution
	PageSize int
	// Values is the results of the last query, or the current page of results if PageSize is smaller than the number of rows returned by the query
	Values ValueSet
	// MorePages is true if there are more pages of results available for fetching
	MorePages bool
	// Err is the latest error experienced by the SQLCommand. If Err is set, then IsValid() will return false.
	Err error
	// Valid is an indicator that an error has occurred. If true, the SQLCommand is OK to use; if not then not.
	valid bool
}

// error returned when Scan called on empty or completed row set
var ErrNoMoreRows = errors.New("No more Rows in result set")

// error returned if an operation is attempted on a result set that isn't populated yet
var ErrNoResult = errors.New("attempted to fetch result values with no valid result")

// error returned if a rollback or commit was called on a SQLCommand with no open transaction
var ErrNoTx = errors.New("attempted to perform a transaction commit or rollback with no transaction open")

/*
the Begin method starts a SQL transaction within the SQLCommand

if the SQLCommand has already errored somewhere, or has no DB reference, it will just return the SQLCommand without doing anything

if the SQLCommand already has a transaction open, Begin will do nothing and continue using the existing transaction
*/
func (sc *SQLCommand) Begin() *SQLCommand {
	//check if we've errored
	if !sc.IsValid() {
		//yep, so just skip this
		return sc
	}
	// check if we've already got a transaction open
	if nil != sc.Tx {
		// just re-use the same Tx, allows us to call begin multiple times to make sure we've got a Tx open
		sc.Log.Printf("re-using existing transaction")
		return sc
	}
	sc.Log.Printf("Starting transaction")
	Tx, err := sc.Db.Begin()
	if nil != err {
		sc.Err = errors.New("error starting transaction: " + err.Error())
		if nil != Tx {
			//if we managed to start one anyway, roll it back
			Tx.Rollback()
		}
		return sc
	}
	//all good, save the transaction and return
	sc.Tx = Tx
	return sc
}

/*
the Prepare method prepares the SQL statement

If the SQLCommand has a transaction open, it will use that. If not then it won't start one.

If the SQLCommand has already errored, it does nothing

If the SQL statement errors, Prepare will set SQLCommand.Err and return the SQLCommand as normal
*/
func (sc *SQLCommand) Prepare() *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	var err error
	var Ps *sql.Stmt

	if nil == sc.Tx {
		//no Tx
		sc.Log.Printf("preparing statement: " + sc.Sql)
		Ps, err = sc.Db.Prepare(sc.Sql)
	} else {
		sc.Log.Printf("  preparing statement: " + sc.Sql)
		Ps, err = sc.Tx.Prepare(sc.Sql)
	}
	if err != nil {
		sc.Err = errors.New("Error preparing SQL: " + err.Error())
		return sc
	}
	sc.Ps = Ps
	return sc
}

/*
the Exec method will execute a prepared statement if there is one and populate the SQLCommand.Res property with the result

if there is no prepared statement, then Exec will execute the SQL without preparing a statement

if there is a transaction, Exec will use it (with or without a prepared statement) but will not create one if not

WARNING: I've had problems with the SQLlite3 database (or driver) either locking or creating handle-less transactions (and not closing them)
with Exec(). Please comment on the bitbucket repo if you find any solutions ;)
*/
func (sc *SQLCommand) Exec(varargs ...interface{}) *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	var result sql.Result
	switch {
	case nil != sc.Ps:
		sc.Log.Printf("  Executing prepared statement with args: %#v", varargs)
		result, sc.Err = sc.Ps.Exec(varargs...)
	case nil != sc.Tx:
		sc.Log.Printf("  Executing transaction raw sql: %q with args: %#v", sc.Sql, varargs)
		result, sc.Err = sc.Tx.Exec(sc.Sql, varargs...)
	default:
		sc.Log.Printf("Executing raw sql: %q with args: %#v", sc.Sql, varargs)
		result, sc.Err = sc.Db.Exec(sc.Sql, varargs...)
	}
	sc.Res = &result
	return sc
}

/*
the Query method will execute a prepared query and populate Rows and ValueSet with the results (unless an error occurs)

if there is a prepared statement, it will use it. If not, it will run the SQL with no prepared statement

if there is an open transaction, it will use it. If not, it won't.
*/
func (sc *SQLCommand) Query(varargs ...interface{}) *SQLCommand {
	if !sc.IsValid() {
		return sc
	}

	var result *sql.Rows
	var err error
	switch {
	case nil != sc.Ps:
		sc.Log.Printf("  Executing prepared query using args: %#v", varargs)
		result, err = sc.Ps.Query(varargs...)
	case nil != sc.Tx:
		sc.Log.Printf("  Executing transaction query %q using args: %#v", sc.Sql, varargs)
		result, err = sc.Tx.Query(sc.Sql, varargs...)
	default:
		sc.Log.Printf("Executing raw SQL query %q using args: %#v", sc.Sql, varargs)
		result, err = sc.Db.Query(sc.Sql, varargs...)
	}
	sc.Err = err
	sc.Rows = result

	return sc
}

/*
the CloseStatement method will close any prepared statement that is open

it ignores the IsValid state, so will attempt to close invalid statements (which is OK)
*/
func (sc *SQLCommand) CloseStatement() *SQLCommand {
	if nil != sc.Ps {
		sc.Log.Printf("closing statement")
		sc.Ps.Close()
		sc.Ps = nil
	}
	return sc
}

/*
the CloseAll method will tidy up behind a SQLCommand and make sure it's all closed and happy.

CloseAll should always be called and is safe to defer (it can be called multiple times with no badness seeping out)
*/
func (sc *SQLCommand) CloseAll() *SQLCommand {
	// close any rows or results open
	if nil != sc.Rows {
		sc.Rows.Close()
		sc.Rows = nil
	}
	sc.Res = nil
	// make sure the statement is closed
	sc.CloseStatement()
	// close the transaction if open... assume if no errors happened we're OK to commit, otherwise roll back
	var errstate error = sc.Err
	if sc.IsValid() {
		sc.Commit()
	} else {
		sc.Rollback()
	}
	//clear any error if it's ErrNoTx as we don't care for the purposes of CloseAll - it needs to be safe to call on any state
	// also restore any previous error, so we don't overwrite valuable error conditions by calling rollback
	if ErrNoTx == sc.Err {
		sc.Err = errstate
	}
	// and let's kill it to be sure
	sc.Tx = nil
	return sc
}

// the Commit method will commit any open transaction
//
// it will set SQLCommand.Err to ErrNoTx if there's no transaction to commit
//
// if the SQLCommand is invalid, it won't do anything (call CloseAll to ensure transactions are closed)
func (sc *SQLCommand) Commit() *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	if nil != sc.Tx {
		sc.Log.Printf("Committing transaction")
		sc.Tx.Commit()
		sc.Tx = nil
	} else {
		sc.Err = ErrNoTx
	}
	return sc
}

// the Rollback method will roll back any open transaction
//
// it will set SQLCommand.Err to ErrNoTx if there's no transaction open
//
// if the SQLCommand is invalid, it will still attempt to close the transaction (if there is one)
func (sc *SQLCommand) Rollback() *SQLCommand {
	//don't check for validity... we're probably rolling back because of an error
	if sc.Rows != nil {
		sc.Rows.Close()
	}
	if sc.Tx != nil {
		sc.Log.Printf("Rolling back transaction")
		sc.Tx.Rollback()
		sc.Tx = nil
	} else {
		sc.Err = ErrNoTx
	}
	return sc
}

// the IsValid method indicates whether the operation chain succeeded or failed
//
// it checks the database on first access to check if it's a valid, open database by pinging it
//
// if the ping fails, any futher checks will also ping the database until it succeeds... at which point any further calls to IsValid will not ping the db
//
// if the db is allowed to timeout, SQLCommand will not reopen it or ping it again (because that may be driver-dependant)
func (sc *SQLCommand) IsValid() bool {
	if sc.Err != nil {
		sc.valid = false
		return false
	}
	// if true, then it must have checked the db already, no need to check again
	if sc.valid {
		return true
	}
	// special state: err is nil and valid is false: first time we've run IsValid on a new command object, so check the db
	err := sc.Db.Ping()
	if nil != err {
		sc.Err = err
		sc.valid = false
	} else {
		sc.valid = true
	}
	return sc.valid
}

// GetLastId populates the LastId property value with the id of the last inserted row from an exec operation (usually INSERT statements)
//
// this function must be called before the SQLCommand.LastId property is available for use
//
// Note: this doesn't work in Postgres because the driver doesn't support this; the workaround is to use a RETURNING clause and do the command as a Query not an Exec
// see: https://groups.google.com/forum/#!topic/golang-nuts/PkOuHNFhmKY
func (sc *SQLCommand) GetLastId() *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	if nil == sc.Res {
		sc.Err = ErrNoResult
		return sc
	}
	lii, err := (*sc.Res).LastInsertId()
	if nil != err {
		sc.Err = errors.New("error getting last insert id: " + err.Error())
		return sc
	}
	sc.LastId = lii
	return sc
}

// GetRowsAffected populates the RowsAffected property on the SQLCommand with the number of rows affected by the last operation
func (sc *SQLCommand) GetRowsAffected() *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	if nil == sc.Res {
		sc.Err = ErrNoResult
		return sc
	}
	ra, err := (*sc.Res).RowsAffected()
	if nil != err {
		sc.Err = errors.New("error getting Rows Affected: " + err.Error())
		return sc
	}
	sc.RowsAffected = ra
	return sc
}

// Scan scans a single row of results into a set of pointers
//
// the pointers must match the database types and be in the same order (see database/sql Scan() documentation - this is just a wrapper)
func (sc *SQLCommand) Scan(args ...interface{}) *SQLCommand {
	// implemented as a wrapper around a private method so ScanPage can use the private version too
	err := sc.scan(args...)
	if ErrNoMoreRows != err {
		sc.Err = err
	}
	return sc
}

// scan is private and scans a single row of results into the pointers provided
func (sc *SQLCommand) scan(args ...interface{}) error {
	if nil != sc.Rows {
		if sc.Rows.Next() {
			err := sc.Rows.Scan(args...)
			return err
		}
		return ErrNoMoreRows
	}
	return ErrNoResult
}

// ScanPage scans the results from a Query and populates ValueSet with the results
//
// if PageSize is >0, it will only populate a maximum of that many rows, call ScanPage again to get the next page
// if PageSize is <1, it will be set to 10000 to conserve memory
func (sc *SQLCommand) ScanPage() *SQLCommand {
	if !sc.IsValid() {
		return sc
	}
	if nil == sc.Rows {
		return sc
	}
	if 1 > sc.PageSize {
		//maxRows = 1<<32 - 1 // slightly stupid, as this is a ludicrously large row set, but it'll do as 'no maximum' and it guarantees to fit in an int
		//new default, because we'll be defining an array with this value
		sc.PageSize = 10000
	}
	cols, err := sc.Rows.Columns()
	if nil != err {
		// unusable result set, better report it
		sc.Log.Println("error returning the column names in the result set: " + err.Error())
		sc.Err = err
		return sc
	}
	sc.Values = ValueSet{
		ValueRows: make([]ValueRow, 0, sc.PageSize),
		Columns:   make([]string, len(cols)),
		Names:     make(map[string]int),
		RowCount:  0,
	}

	for i, n := range cols {
		sc.Values.Columns[i] = n
		sc.Values.Names[n] = i
	}

	vals := make([]interface{}, len(cols))
	for i := 0; i < len(cols); i++ {
		vals[i] = new(interface{})
	}
	var morepages bool = true
	for i := 0; nil == err && sc.PageSize > i; i++ {
		err = sc.scan(vals...)
		if nil == err {
			ab := make(ValueRow, len(cols))
			copyValues(ab, vals, nil)
			sc.Values.ValueRows = append(sc.Values.ValueRows, ab)
		}

	}
	if ErrNoMoreRows == err {
		// ignore ErrNoMoreRows as that just indicates we got to the end of the results
		morepages = false
		sc.Err = nil
	} else {
		// bad thing happened so keep the error
		sc.Err = err
	}
	sc.MorePages = morepages
	sc.Values.RowCount = len(sc.Values.ValueRows)
	return sc
}

func copyValues(dst []interface{}, src []interface{}, lg *log.Logger) {
	for k, rv := range src {
		if nil != lg {
			lg.Printf("column %d has raw value: %v", k, rv)
		}
		rvl := reflect.ValueOf(rv)
		if nil != lg {
			lg.Printf("column %d has value: %v", k, rvl)
		}
		rvi := reflect.Indirect(rvl)
		for rvi != reflect.Indirect(rvi) {
			rvi = reflect.Indirect(rvi)
		}
		if nil != lg {
			lg.Printf("column %d has indirect: %v", k, rvi)
			lg.Printf("column %d has interface: %v", k, rvi.Interface())
		}
		//check for zero value
		if rvi.IsValid() {
			dst[k] = rvi.Interface()
		} else {
			dst[k] = nil
		}
	}
}
