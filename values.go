package sqlcommand

import

// ValueRow is a type mapping to hold a single row of query results
// All results are returned as a slice of interface{} containing VALUES (not pointers) of the results
"errors"

type ValueRow []interface{}

// ValueSet holds the results of a query
type ValueSet struct {
	ValueRows []ValueRow
	Columns   []string
	Names     map[string]int
	RowCount  int
}

var ErrInvalidColumnName = errors.New("Invalid Column Name")
var ErrNoRow = errors.New("Invalid Row Index")
var ErrUnableToConvert = errors.New("Unable to convert value to target type")
var ErrTargetTooSmall = errors.New("Target array is too small to contain the value")

func (vs *ValueSet) GetStringValue(row int, col string, value *string) error {
	if (vs.RowCount < row) || (row < 0) {
		return ErrNoResult
	}
	c, ok := vs.Names[col]
	if !ok {
		return ErrInvalidColumnName
	}
	v := vs.ValueRows[row][c]
	if nil == v {
		return ErrNoRow
	}
	r, ok := v.(string)
	if !ok {
		return ErrUnableToConvert
	}
	*value = r
	return nil
}
func (vs *ValueSet) GetIntValue(row int, col string, value *int64) error {
	if (vs.RowCount < row) || (row < 0) {
		return ErrNoResult
	}
	c, ok := vs.Names[col]
	if !ok {
		return ErrInvalidColumnName
	}
	v := vs.ValueRows[row][c]
	if nil == v {
		return ErrNoRow
	}
	r, ok := v.(int64)
	if !ok {
		return ErrUnableToConvert
	}
	*value = r
	return nil
}

func (vs *ValueSet) GetFloatValue(row int, col string, value *float64) error {
	if (vs.RowCount < row) || (row < 0) {
		return ErrNoResult
	}
	c, ok := vs.Names[col]
	if !ok {
		return ErrInvalidColumnName
	}
	v := vs.ValueRows[row][c]
	if nil == v {
		return ErrNoRow
	}

	r, ok := v.(float64)
	if !ok {
		return ErrUnableToConvert
	}
	*value = r
	return nil
}
func (vs *ValueSet) GetBytesValue(row int, col string, value *[]byte) error {
	if (vs.RowCount < row) || (row < 0) {
		return ErrNoResult
	}
	c, ok := vs.Names[col]
	if !ok {
		return ErrInvalidColumnName
	}
	v := vs.ValueRows[row][c]
	if nil == v {
		return ErrNoRow
	}
	r, ok := v.([]byte)
	if !ok {
		return ErrUnableToConvert
	}
	if len(r) > cap(*value) {
		//can't fit!
		return ErrTargetTooSmall
	}
	*value = r
	return nil
}
