package sqlcommand

import (
	"database/sql"
	"log"
	"os"
	"sync"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB
var l *log.Logger

var runDbOnce sync.Once = sync.Once{}

func init() {
	runDbOnce.Do(CreateTestDb)
}

func CreateTestDb() {
	os.Remove("test.db")
	var err error

	db = openDb()
	err = db.Ping()
	if nil != err {
		panic("unable to ping database")
	}
	l = log.New(os.Stdout, "SQLCommandTesting -:- ", 0)
}
func openDb() *sql.DB {
	db, err := sql.Open("sqlite3", "test.db")
	if nil != err {
		panic("unable to open database because: " + err.Error())
	}
	return db
}
func TestNewFactory(t *testing.T) {

	scf := NewFactory(db, l)
	err := scf.Db.Ping()
	if nil != err {
		t.Fatal("unable to ping command factory db because: " + err.Error())
	}
	scf.Log.Output(1, "testing logger")
}

func TestNewCommand(t *testing.T) {
	scf := NewFactory(db, l)
	sql := "create table foo (id integer not null primary key, name text);"
	sc := scf.NewCommand(sql)
	if !sc.IsValid() {
		t.Fatal("Created invalid command")
	}
	if !sc.Begin().Rollback().IsValid() {
		t.Fatal("failed to chain an empty transaction")
	}
}

func ExampleSQLCommandFactory() {
	db, err := sql.Open("sqlite3", "example.db")
	if nil != err {
		panic("unable to open database")
	}
	l := log.New(os.Stdout, "SQLCommand - ", log.LstdFlags)
	scf := NewFactory(db, l)
	sc := scf.NewCommand("Select id,name from foo")
	sc.Begin().Prepare().Query().CloseAll()
	if !sc.IsValid() {
		panic("query failed because: " + sc.Err.Error())
	}
	if 0 < sc.Values.RowCount {
		log.Printf("first id is: %s, belonging to: %s", sc.Values.ValueRows[0][sc.Values.Names["id"]], sc.Values.ValueRows[0][sc.Values.Names["name"]])
	} else {
		log.Print("table foo is empty")
	}
}
