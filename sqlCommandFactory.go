package sqlcommand

import (
	"database/sql"
	"log"
)

/*
SQLCommandFactory provides an easy way of building SQLCommands without having to specify the database or log each time
*/
type SQLCommandFactory struct {
	Db              *sql.DB
	Log             *log.Logger
	DefaultPageSize int
}

//NewCommand builds a new command using the factory's database, log and default page size
func (scf *SQLCommandFactory) NewCommand(sql string) *SQLCommand {
	r := new(SQLCommand)
	r.Db = scf.Db
	r.Log = scf.Log
	r.Sql = sql
	r.PageSize = scf.DefaultPageSize
	return r
}

//NewFactory creates a new SQLCommandFactory and sets a default page size of 100
func NewFactory(db *sql.DB, l *log.Logger) *SQLCommandFactory {
	return &SQLCommandFactory{
		Db:              db,
		Log:             l,
		DefaultPageSize: 100,
	}
}
