package sqlcommand

import (
	"reflect"
	"testing"
)

var scf SQLCommandFactory

func init() {
	runDbOnce.Do(CreateTestDb)
	scf = *NewFactory(db, l)
}

func TestCreateDb(t *testing.T) {
	sql := "create table foo (id integer not null primary key, name text);"
	sc := scf.NewCommand(sql)
	sc.Begin().Exec().CloseAll()
	if !sc.IsValid() {
		t.Fatal("create table failed because: " + sc.Err.Error())
	}
}

func TestInsertRows(t *testing.T) {
	sql := "insert into foo (id,name) values(?,?)"
	sc := scf.NewCommand(sql)
	sc.Begin().Exec(1, "testInsertRows").GetLastId().GetRowsAffected().CloseAll()
	if !sc.IsValid() {
		t.Fatal("insert rows failed because: " + sc.Err.Error())
	}
	if 1 != sc.LastId {
		t.Fatal("last id wasn't 1")
	}
	if 1 != sc.RowsAffected {
		t.Fatal("rows affected wasn't 1")
	}
	//test insert a second row using a variable and no transaction
	nid := sc.LastId + 5
	sc.Exec(nid, "test insert another row").GetLastId().GetRowsAffected().CloseAll()
	if !sc.IsValid() {
		t.Fatal("insert rows failed because: " + sc.Err.Error())
	}
	if nid != sc.LastId {
		t.Fatalf("last id was %d exepcting %d", sc.LastId, nid)
	}
	if 1 != sc.RowsAffected {
		t.Fatal("more than 1 row affected by the insert")
	}
}

func TestUpdateRow(t *testing.T) {
	sql := "update foo set name=? where id=?"
	sc := scf.NewCommand(sql)
	sc.Prepare().Exec("changed row name", 1).GetRowsAffected().CloseAll()
	if !sc.IsValid() {
		t.Fatal("update rows failed because: " + sc.Err.Error())
	}
	if 1 != sc.RowsAffected {
		t.Fatalf("update affected %d rows ", sc.RowsAffected)
	}
}

func TestQuerySingleRow(t *testing.T) {
	sql := "select * from foo where id=?"
	sc := scf.NewCommand(sql)

	sc.Begin().Query(1).ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("Query failed with: " + sc.Err.Error())
	}
	t.Logf("values set is %d elements long", sc.Values.RowCount)
	if sc.Values.RowCount != len(sc.Values.ValueRows) {
		t.Fatalf("rowcount of %d doesn't match actual rows of %d", sc.Values.RowCount, len(sc.Values.ValueRows))
	}
	if sc.Values.RowCount > 1 {
		t.Fatalf("rowcount is > 1 on a single-id query: %d ", sc.Values.RowCount)
	}
	for i, n := range sc.Values.Columns {
		t.Logf("column %d is called '%s'", i, n)
	}
	for n, i := range sc.Values.Names {
		t.Logf("column '%s' has index %d", n, i)
	}
	colid := sc.Values.Names["id"]
	colname := sc.Values.Names["name"]
	if colid == colname {
		t.Fatal("id and name have the same column index")
	}
	t.Logf("ValueRows: %v", sc.Values.ValueRows)
	for i, r := range sc.Values.ValueRows {
		t.Logf("range member is: %#v", r)
		var ok bool
		t.Logf("attempting to convert -%v- to int", r[colid])
		id, ok := r[colid].(int64)
		if !ok {
			k := reflect.ValueOf(r[colid]).Kind().String()
			t.Fatalf("type assertion failed on id field. Id is %v, kind is %s result is %d", r[colid], k, id)
		}
		name, ok := r[colname].(string)
		if !ok {
			t.Fatalf("type assertion failed on name field. Name is %#v", r[colname])
		}
		t.Logf("row %d, %d, %s", i, id, name)
	}
	// test with Statement
	sc.Begin().Prepare().Query(1).ScanPage().CloseStatement().Commit()
	if !sc.IsValid() {
		t.Fatal("Query with statement failed because: " + sc.Err.Error())
	}
	// test with no tx
	sc.Query(1).ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("Query with no transaction failed because: " + sc.Err.Error())
	}
	// test with no tx but a statement
	sc.Prepare().Query(1).ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("QueryRow with no tx but a statement failed because: " + sc.Err.Error())
	}
	// test Commit failing with no tx
	sc.Prepare().Query(1).ScanPage().CloseStatement().Commit()
	if sc.IsValid() {
		t.Fatal("QueryRow failed to error with commit() on no transaction")
	}

}

func TestNoResults(t *testing.T) {
	sql := "select * from foo where id=?"
	sc := scf.NewCommand(sql)
	sc.Query(34534534).ScanPage().CloseAll()
	if nil != sc.Err {
		t.Fatal("error on blank query: " + sc.Err.Error())
	}
	if sc.Values.RowCount != 0 {
		t.Fatalf("Expected 0 rows returned, got %d", sc.Values.RowCount)
	}
}

func TestBadSQL(t *testing.T) {
	sql := "this is bad sql"
	sc := scf.NewCommand(sql)
	sc.Query().CloseAll()
	if sc.IsValid() {
		t.Fatal("no error returned for bad sql in Query")
	}
	sc.Err = nil
	sc.Exec().CloseAll()
	if sc.IsValid() {
		t.Fatal("no error returned for bad sql in Exec")
	}
	sc.Err = nil
	sc.Prepare()
	if sc.IsValid() {
		t.Fatal("no error returned for bad sql in Prepare")
	}
}

func TestQuery(t *testing.T) {
	sql := "select * from foo"
	sc := scf.NewCommand(sql)

	sc.Begin().Query().ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("Query errored with a transaction: " + sc.Err.Error())
	}
	if sc.Values.RowCount != len(sc.Values.ValueRows) {
		t.Fatalf("reported RowCount (%d) doesn't actually match count of rows (%d)", sc.Values.RowCount, len(sc.Values.ValueRows))
	}
	t.Logf("result set is %d elements long", sc.Values.RowCount)
	colid := sc.Values.Names["id"]
	colname := sc.Values.Names["name"]
	if colid == colname {
		t.Fatal("One of the columns isn't in the name lookup")
	}
	for i, r := range sc.Values.ValueRows {
		t.Logf("range member is: %#v", r)

		id, ok := r[colid].(int64)
		if !ok {
			t.Fatalf("type assertion failed on id field. Id is %s", r[0])
		}

		name, ok := r[colname].(string)
		if !ok {
			t.Fatalf("type assertion failed on name field. Name is %s", r[1])
		}
		t.Logf("row %d, %d, %s", i, id, name)
	}
	// gotta get that test coverage figure up!
	// no transaction, no statement, limited row result set
	sc.Query().ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("Query errored with no transaction: " + sc.Err.Error())
	}
	if 2 != sc.Values.RowCount {
		t.Fatalf("Query maxrows didn't work as planned, supposed to return 2 rows, instead returned %d rows", sc.Values.RowCount)
	}

}
func TestPageSize(t *testing.T) {
	sc := scf.NewCommand("select * from foo")
	sc.PageSize = 0
	sc.Query().ScanPage().CloseAll()
	if !sc.IsValid() {
		t.Fatal("Error on small pagesize: " + sc.Err.Error())
	}
	if 10000 != sc.PageSize {
		t.Fatalf("Expected pagesize of 10000, got %d", sc.PageSize)
	}
}

func TestScan(t *testing.T) {
	sc := scf.NewCommand("select id,name from foo where id=?")
	sc.Query(1)
	defer sc.CloseAll()
	id := new(int)
	name := new(string)
	sc.Scan(id, name)
	if 1 != *id {
		t.Logf("id: %#v", id)
		t.Logf("name: %#v", name)
		t.Logf("")
		t.Fatalf("expected scan result of 1, got %d", *id)
	}
}
func TestCopyValues(t *testing.T) {
	a := make([]interface{}, 3)
	b := 5
	c := "test string"
	a[0] = &b
	a[1] = &c
	a[2] = 2

	ac := make([]interface{}, 3)
	copyValues(ac, a, nil)
	t.Logf("old array: %#v", a)
	t.Logf("new array: %#v ", ac)
	a[0] = 1
	a[1] = 2
	a[2] = 3
	t.Logf("old array: %#v", a)
	t.Logf("new array: %#v ", ac)

}

func ExampleSQLCommand() {
	requiredId := 1
	type exampleStruct struct {
		id   int
		name string
	}
	result := new(exampleStruct)
	sc := scf.NewCommand("Select id,name from foo where id=?")

	// now we can actually run the query
	sc.Query(requiredId).ScanPage().CloseAll()
	if !sc.IsValid() {
		// don't actually panic about this... handle the error
		panic("query errored with: " + sc.Err.Error())
	}
	result.id = sc.Values.ValueRows[0][0].(int)
	result.name = sc.Values.ValueRows[0][sc.Values.Names["name"]].(string)

	// or you can use Scan to get the values directly to the result struct
	if !sc.Query(requiredId).Scan(&result.id, &result.name).CloseAll().IsValid() {
		panic("queryRow errored with: " + sc.Err.Error())
	}
}
