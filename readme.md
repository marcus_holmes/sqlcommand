#SQLCommand library
--------

Marcus Holmes 2013

This library reduces the boilerplate involved in handling databases in Go

it wraps some of the standard methods in the database/sql library so that they can be chained together
and any error conditions dealt with afterwards

Instead of the standard:
```Go
db, err := sql.Open(driver,dataSourceName)
if err != nil{
	panic("bad thing happened")
}
tx,err := db.Begin()
if err != nil{
	panic("transaction fail" + err.Error())
}
ps,err := tx.Prepare(sqlStatement)
if err != nil{
	tx.Rollback()
	panic("statement fail: " + err.Error())
}
defer ps.Close()
err = ps.Query(value1,value2,value3)
if err != nil{
	tx.Rollback()
	panic("query fail")
}
results := map[int]resultStruct
pattern := new(resultStruct)
for err == nil{
	err = ps.Scan(&pattern.field1,&pattern.field2,&pattern.field3)
	if err == nil{
		rs := resultStruct{
			field1: pattern.field1,
			field2: pattern.field2,
			field3: pattern.field3,
		}
		results[rs.field1] = rs
	}
}
if err != sql.ErrNoRows{
	tx.Rollback()
	panic("result retrieval fail: " + err.Error())
}
ps.Close()
tx.Commit()
DoSomethingWithResults(results)
```
SQLCommand allows you to do:

```Go
db := sql.Open(driver,dataSourceName)
lg := log.New(os.Stdout, "SQL: ", log.StdFlags)
scf := NewFactory(db,lg)
sc := scf.NewCommand(sqlStatement)
results := make(ResultSet)
pattern := new(resultStruct)
sc.Begin().Prepare()
sc.Query(value1,value2,value3).ScanAll(results,-1,&pattern.field1,&pattern.field2,&pattern.field3).CloseAll()
if !sc.IsValid(){
	panic("sql fail: " + sc.Err.Error())
}
// slight cheat: the results are in a slice of sqlcommand.ResultRow structs, not the resultStruct in the first example
DoSomethingWithResults(results) 
```

Hopefully the benefits are clear!

Any feedback is welcome, please leave comments and suggestions for improvements

