package sqlcommand

import (
	"testing"
)

var tv ValueSet
var s = "test string"
var i int64 = 4565465464564564
var f float64 = 546456456.75674534
var b = []byte{1, 2, 3, 4, 5}

func init() {
	// build a test set of values
	tv = ValueSet{
		ValueRows: make([]ValueRow, 1),
		Columns:   []string{"string", "int", "float", "byte"},
		Names:     make(map[string]int),
		RowCount:  1,
	}
	tv.ValueRows[0] = make([]interface{}, 4)
	//string

	tv.ValueRows[0][0] = s
	//int

	tv.ValueRows[0][1] = i
	//float

	tv.ValueRows[0][2] = f
	//bytes

	tv.ValueRows[0][3] = b

	// column names
	tv.Names["string"] = 0
	tv.Names["int"] = 1
	tv.Names["float"] = 2
	tv.Names["byte"] = 3

}
func TestGetStringValue(t *testing.T) {
	val := new(string)

	// invalid value set
	newtv := ValueSet{}
	err := newtv.GetStringValue(0, "string", val)
	if nil == err {
		t.Fatal("GetStringValue didn't error on invalid value")
	}
	// now an invalid row
	err = tv.GetStringValue(10, "string", val)
	if nil == err {
		t.Fatal("GetStringValue didn't error on invalid row")
	}
	// now an invalid column
	err = tv.GetStringValue(0, "this is bollocks", val)
	if nil == err {
		t.Fatal("GetStringValue didn't error on invalid column")
	}
	//unable to convert
	err = tv.GetStringValue(0, "float", val)
	if nil == err {
		t.Fatal("GetStringValue didn't error on invalid convert")
	}

	// now a valid case
	err = tv.GetStringValue(0, "string", val)
	if nil != err {
		t.Fatal("error on valid GetStringValue: " + err.Error())
	}
}

func TestGetIntValue(t *testing.T) {
	val := new(int64)

	/// invalid value set
	newtv := ValueSet{}
	err := newtv.GetIntValue(0, "int", val)
	if nil == err {
		t.Fatal("GetIntValue didn't error on invalid value")
	}
	// now an invalid row
	err = tv.GetIntValue(10, "int", val)
	if nil == err {
		t.Fatal("GetIntValue didn't error on invalid row")
	}
	// now an invalid column
	err = tv.GetIntValue(0, "this is bollocks", val)
	if nil == err {
		t.Fatal("GetIntValue didn't error on invalid column")
	}
	//unable to convert
	err = tv.GetIntValue(0, "string", val)
	if nil == err {
		t.Fatal("GetIntValue didn't error on invalid convert")
	}

	// now a valid case
	err = tv.GetIntValue(0, "int", val)
	if nil != err {
		t.Fatal("error on valid GetIntValue: " + err.Error())
	}
}

func TestGetFloatValue(t *testing.T) {
	val := new(float64)

	// invalid value set
	newtv := ValueSet{}
	err := newtv.GetFloatValue(0, "float", val)
	if nil == err {
		t.Fatal("GetFloatValue didn't error on invalid value")
	}
	// now an invalid row
	err = tv.GetFloatValue(10, "float", val)
	if nil == err {
		t.Fatal("GetFloatValue didn't error on invalid row")
	}
	// now an invalid column
	err = tv.GetFloatValue(0, "this is bollocks", val)
	if nil == err {
		t.Fatal("GetFloatValue didn't error on invalid column")
	}
	//unable to convert
	err = tv.GetFloatValue(0, "string", val)
	if nil == err {
		t.Fatal("GetFloatValue didn't error on invalid convert")
	}

	// now a valid case
	err = tv.GetFloatValue(0, "float", val)
	if nil != err {
		t.Fatal("error on valid GetFloatValue: " + err.Error())
	}
}

func TestGetBytesValue(t *testing.T) {
	val := make([]byte, 10)

	// invalid value set
	newtv := ValueSet{}
	err := newtv.GetBytesValue(0, "byte", &val)
	if nil == err {
		t.Fatal("GetByteValue didn't error on invalid value")
	}
	// now an invalid row
	err = tv.GetBytesValue(10, "byte", &val)
	if nil == err {
		t.Fatal("GetByteValue didn't error on invalid row")
	}
	// now an invalid column
	err = tv.GetBytesValue(0, "this is bollocks", &val)
	if nil == err {
		t.Fatal("GetByteValue didn't error on invalid column")
	}
	//unable to convert
	err = tv.GetBytesValue(0, "float", &val)
	if nil == err {
		t.Fatal("GetByteValue didn't error on invalid convert")
	}
	//special case: small byte array
	small := make([]byte, 1)
	err = tv.GetBytesValue(0, "byte", &small)
	if nil == err {
		t.Fatal("GetByteValue didn't error on tiny target")
	}

	// now a valid case
	err = tv.GetBytesValue(0, "byte", &val)
	if nil != err {
		t.Fatal("error on valid GetByteValue: " + err.Error())
	}
}
